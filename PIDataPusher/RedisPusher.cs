﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using MLJSystems.Pi.Simulation;
using StackExchange.Redis;
using Exception = System.Exception;

namespace PIDataPusher
{
    public class RedisPusher
    {
        private string _filename;
        private BackgroundWorker worker;
        private IDatabase _redisDb;
        private PISimulator _piSimulator;

        public RedisPusher(string filename, string connectionString, int database)
        {
            try
            {
                Constructor(filename, ConnectionMultiplexer.Connect(connectionString).GetDatabase(database));
            }
            catch (Exception e)
            {
                throw new Exception("Unable to connect to redis" ,e);
            }
           
        }

        public RedisPusher(string filename, IConnectionMultiplexer connection, int database)
        {
            Constructor(filename, connection.GetDatabase(database));
        }

        public RedisPusher(string filename, IDatabase redisDb)
        {
            Constructor(filename, redisDb);
        }

        private void Constructor(string filename, IDatabase redisDb)
        {
            //_redisDb = redisDb;
            _piSimulator = new PISimulator(filename);
            worker = new BackgroundWorker();
            worker.DoWork += WorkerOnDoWork;
        }

        private void WorkerOnDoWork(object sender, DoWorkEventArgs e)
        {
            if (_redisDb != null && _piSimulator != null) 
            {
                //while (!e.Cancel)
                //{
                    
                //}
            } 
        }


        public void Start()
        {
            if (!worker.IsBusy)
            {
               worker.RunWorkerAsync(); 
            }
        }

        public void Cancel()
        {
            if (worker.IsBusy)
            {
                worker.CancelAsync();
            }
        }


    }
}
