﻿namespace PIDataRecorder.Core.Models
{
    public class ChargerCrane
    {
        /* vnx sourced variables */
        public double Postion { get; set; }
        public double Weight { get; set; }
        /* non VXN variables */
        public double V1PositionCount { get; set; }
        public double V2PositionCount { get; set; }
        //public double weight_count { get; set; }
        public double WeightDecreasing { get; set; }
        public bool ReportNextMovement { get; set; }
        public double StoredChargingWeight { get; set; }
        public int StoredVessel { get; set; }
        public bool ChargingReported { get; set; }
    }
}
