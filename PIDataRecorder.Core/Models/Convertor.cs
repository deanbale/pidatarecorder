﻿namespace PIDataRecorder.Core.Models
{
    public class Convertor
    {
        /* vnx sourced variables */
        public double BapSequenceStep{ get; set; }
        public double OgStartLamp{ get; set; }
        public double OgIgnitionLamp{ get; set; }
        public double OgManualModeLamp{ get; set; }
        public double OgAutoModeLamp{ get; set; }
        public double OgBurnIn{ get; set; }
        public double OgPvdFlowControl{ get; set; }
        public double BosTiltAngle{ get; set; }
        public double LanceHeight{ get; set; }
        public double OxygenFlow{ get; set; }
        public double NitrogenFlow{ get; set; }
        public double FluxMixing{ get; set; }
        public double SlagWashWide{ get; set; }
        public double BapSubStep{ get; set; }
        public double ScrapRocking{ get; set; }
        public double Heartbeat{ get; set; }
        /* non VXN variables */
        public double AtSlagCount{ get; set; }
        public double AtScrapCount{ get; set; }
        public double AtHotMetalCount{ get; set; }
        public double AtVerticalCount{ get; set; }
        public double AtTapCount{ get; set; }
        public double AtDwellCount{ get; set; }
        public double OxygenFlowCount{ get; set; }
        public double NitrogenFlowCount{ get; set; }
        public double LanceHeightCount{ get; set; }
        public double BapSeqStepCount{ get; set; }
        public double BapSubStepCount{ get; set; }
        public double Other{ get; set; }
    }
}
