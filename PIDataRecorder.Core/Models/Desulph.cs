﻿namespace PIDataRecorder.Core.Models
{
    public class Desulph
    {
        /* vnx sourced variables */
        public double Treating{ get; set; }
        public double Injecting{ get; set; }
        public double Rabbling{ get; set; }
        public double LadleTilt{ get; set; }
        public double HoodSpeed{ get; set; }
        public bool HoodClosed{ get; set; }
        public bool HoodOpen{ get; set; }
        /* non VXN variables */
        public double TreatmentCount{ get; set; }
        public double RabblingCount{ get; set; }
        public double HoodMoving{ get; set; }
        public double HoodPosition{ get; set; }
        public bool HoodOn{ get; set; }
        public bool HoodParked{ get; set; }
    }
}
