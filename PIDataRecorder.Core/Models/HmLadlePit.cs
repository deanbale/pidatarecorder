﻿namespace PIDataRecorder.Core.Models
{
    public class HmLadlePit
    {
        /* vnx sourced variables */
        public double Pouring{ get; set; }
        /* non VXN variables */
        public double InactiveCount{ get; set; }
    }
}
