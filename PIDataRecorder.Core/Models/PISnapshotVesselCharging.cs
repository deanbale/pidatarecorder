﻿using System;

namespace PIDataRecorder.Core.Models
{
    public class PISnapshotVesselCharging
    {
        public DateTime TimeStampUtc { get; set; }
        public Convertor ConvertorOne { get; set; }
        public Convertor ConvertorTwo { get; set; }
        public Desulph DesulphNorth { get; set; }
        public Desulph DesulphSouth { get; set; }
        public HmLadlePit HmLadlePitNorth { get; set; }
        public HmLadlePit HmLadlePitSouth { get; set; }
        public ChargerCrane ChargerCraneNorth { get; set; }
        public ChargerCrane ChargerCraneSouth { get; set; } 
    }
}
