﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Topshelf;

namespace PIDataCollector
{
    class Program
    {
        static void Main(string[] args)
        {

            var fileLocation = @"C:\Users\student2\Documents\Data Files\";
            var filename = fileLocation + $@"PiData-VesselCharging.txt";

            var collector = new Collector(filename);
            if (collector.Collect(DateTime.UtcNow.AddDays(-1).AddHours(-3), DateTime.UtcNow.AddDays(-1)))
            {
                Console.WriteLine("Data recorded");
            }
            else
            {
                Console.WriteLine("Unable to get data");
            }

            Console.ReadLine();
        }
    }
}
