﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using MLJSystems.Pi;
using MLJSystems.Pi.Simulation;
using Newtonsoft.Json;
using PIDataRecorder.Core.Models;

namespace PIDataCollector
{
    public class Collector
    {

        //Data Collection
        private PI _pi;
        private string _filename;

        public Collector(string filename)
        { 
            _filename = filename;

            ConnectToPi();
        }

        public bool Collect(DateTime fromUtc, DateTime toUtc)
        {

            if (_pi != null || ConnectToPi())
            {
                var simFileBuilder = new PISimulationFileBuilder(_pi);
                simFileBuilder.Build(GetPiTags(), fromUtc, toUtc, 1, _filename);
            }
            else
            {
                throw new Exception("Unable to conenct to PI");
            }

            return true;
        }

        private List<string> GetPiTags()
        {
            return new List<string>
            {
                //Convertor One Tags
                "MVSEQSTEPA_OP",
                "GM02H403_2",
                "GM02H403_3",
                "GM02H403_9",
                "GM02H403_10",
                "GM02H2_11",
                "GM02B14_10",
                "GM02H450",
                "GM04H100",
                "MVF210/02A_FL",
                "MVFIC310A_PV",
                "MVSWASH1/A_OP",
                "MVSWASH2/A_OP",
                "MVSUBSTEPA_OP",

                //Convertor Two Tags
               "MVSEQSTEPB_OP",
               "GM03H403_2",
               "GM03H403_3",
               "GM03H403_9",
               "GM03H403_10",
               "GM03H2_11",
               "GM03B14_10",
               "GM03H450",
               "GM05H100",
               //"MVF210S/02B_FL",
               "MVFIC310A_PV",
               "MVSWASH1/B_OP",
               "MVSWASH2/B_OP",
               "MVSUBSTEPB_OP",

               //Desulph North
                "GM10G392_0",
                "GM10G1050_0",
                "GM10H1021_8",
                "GM10G120_2/N",
                "GM10B225",
                "GM10A30_11",
                "GM10A30_10",

                //Desluph South
                "GM10G1892_0",
                "GM10G2550_0",
                "GM10H1022_8",
                "GM10G1620_2/N",
                "GM10B228",
                "GM10A20_10",
                "GM10A20_11",

                //HM ladle pit North
                "GM13H652",

                //HM ladle pit South
                "GM13H651",

                //North Charger
                "nc_position",
                "cws_nc_weight",

                //South Charger
                "sc_position",
                "cws_sc_weight",

            };
        }
        
        private bool CollectSnapshot()
        {

            try
            {

                var convertorOne = new Convertor
                {
                    BapSequenceStep = _pi.GetFloatSnapshot("MVSEQSTEPA_OP"),
                    OgStartLamp = _pi.GetFloatSnapshot("GM02H403_2"),
                    OgIgnitionLamp = _pi.GetFloatSnapshot("GM02H403_3"),
                    OgManualModeLamp = _pi.GetFloatSnapshot("GM02H403_9"),
                    OgAutoModeLamp = _pi.GetFloatSnapshot("GM02H403_10"),
                    OgBurnIn = _pi.GetFloatSnapshot("GM02H2_11"),
                    OgPvdFlowControl = _pi.GetFloatSnapshot("GM02B14_10"),
                    BosTiltAngle = _pi.GetFloatSnapshot("GM02H450"),
                    LanceHeight = _pi.GetFloatSnapshot("GM04H100"),
                    OxygenFlow = _pi.GetFloatSnapshot("MVF210/02A_FL"),
                    NitrogenFlow = _pi.GetFloatSnapshot("MVFIC310A_PV"),
                    FluxMixing = _pi.GetFloatSnapshot("MVSWASH1/A_OP"),
                    SlagWashWide = _pi.GetFloatSnapshot("MVSWASH2/A_OP"),
                    BapSubStep = _pi.GetFloatSnapshot("MVSUBSTEPA_OP"),
                };

                var convertorTwo = new Convertor
                {
                    BapSequenceStep = _pi.GetFloatSnapshot("MVSEQSTEPB_OP"),
                    OgStartLamp = _pi.GetFloatSnapshot("GM03H403_2"),
                    OgIgnitionLamp = _pi.GetFloatSnapshot("GM03H403_3"),
                    OgManualModeLamp = _pi.GetFloatSnapshot("GM03H403_9"),
                    OgAutoModeLamp = _pi.GetFloatSnapshot("GM03H403_10"),
                    OgBurnIn = _pi.GetFloatSnapshot("GM03H2_11"),
                    OgPvdFlowControl = _pi.GetFloatSnapshot("GM03B14_10"),
                    BosTiltAngle = _pi.GetFloatSnapshot("GM03H450"),
                    LanceHeight = _pi.GetFloatSnapshot("GM05H100"),
                    //OxygenFlow = _pi.GetFloatSnapshot("MVF210S/02B_FL"),
                    NitrogenFlow = _pi.GetFloatSnapshot("MVFIC310A_PV"),
                    FluxMixing = _pi.GetFloatSnapshot("MVSWASH1/B_OP"),
                    SlagWashWide = _pi.GetFloatSnapshot("MVSWASH2/B_OP"),
                    BapSubStep = _pi.GetFloatSnapshot("MVSUBSTEPB_OP")
                };


                var desulphNorth = new Desulph
                {
                    Treating = _pi.GetFloatSnapshot("GM10G392_0"),
                    Injecting = _pi.GetFloatSnapshot("GM10G1050_0"),
                    Rabbling = _pi.GetFloatSnapshot("GM10H1021_8"),
                    LadleTilt = _pi.GetFloatSnapshot("GM10G120_2/N"),
                    HoodSpeed = _pi.GetFloatSnapshot("GM10B225"),
                    HoodClosed = _pi.GetBoolSnapshot("GM10A30_11"),
                    HoodOpen = _pi.GetBoolSnapshot("GM10A30_10")
                };

                var desulphSouth = new Desulph
                {
                    Treating = _pi.GetFloatSnapshot("GM10G1892_0"),
                    Injecting = _pi.GetFloatSnapshot("GM10G2550_0"),
                    Rabbling = _pi.GetFloatSnapshot("GM10H1022_8"),
                    LadleTilt = _pi.GetFloatSnapshot("GM10G1620_2/N"),
                    HoodSpeed = _pi.GetFloatSnapshot("GM10B228"),
                    HoodClosed = _pi.GetBoolSnapshot("GM10A20_10"),
                    HoodOpen = _pi.GetBoolSnapshot("GM10A20_11")
                };

               
                var hmLadlePitNorth = new HmLadlePit
                {
                    Pouring = _pi.GetFloatSnapshot("GM13H652")
                };

                var hmLadlePitSouth = new HmLadlePit
                {
                    Pouring = _pi.GetFloatSnapshot("GM13H651")
                };

                var craneNorth = new ChargerCrane
                {
                    Postion = _pi.GetFloatSnapshot("nc_position"),
                    Weight = _pi.GetFloatSnapshot("cws_nc_weight")
                };

                var craneSouth = new ChargerCrane
                {
                    Postion = _pi.GetFloatSnapshot("sc_position"),
                    Weight = _pi.GetFloatSnapshot("cws_sc_weight")
                };


                //_vesselChargingData.Add(new PISnapshotVesselCharging
                //{
                //    TimeStampUtc = DateTime.UtcNow,
                //    ConvertorOne = convertorOne,
                //    ConvertorTwo = convertorTwo,
                //    DesulphNorth = desulphNorth,
                //    DesulphSouth = desulphSouth,
                //    HmLadlePitNorth = hmLadlePitNorth,
                //    HmLadlePitSouth = hmLadlePitSouth,
                //    ChargerCraneNorth = craneNorth,
                //    ChargerCraneSouth = craneSouth
                //});

                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }
        }

       private bool ConnectToPi()
        {
            try
            {
                _pi = new PI("ps_pi");
                return true;
            }
            catch
            {

                _pi = null;
                Console.WriteLine("Failed to connect to PS_PI. ");
                return false;
            }
        }

       

    }
}
